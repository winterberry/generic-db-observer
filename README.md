# generic-db-observer

A generic observer that monitors a database for changes (currently only Redis is supported) and then triggers a change in some Event Subject.
To create a program based on this library, you need to implement `EventSubject`, and then run the function `ObserverConfig::run_observer`.

### Limitations:
- Redis PubSub doesn't actually tell us which channels are added or left, instead we receive a notification that "something has changed", and then we have to load the entire set of tracked channels and compare it with which channels are currently joined. This probably causes quite a performance hit.
- The Event Subject connection is expected to be blocking, whereas the Database connection is expected to be async.
