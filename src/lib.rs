//! # generic-db-observer
//!
//! A generic observer that monitors a database for changes (currently only Redis is supported) and then triggers a change in some Event Subject.
//! To create a program based on this library, you need to implement [`EventSubject`], and then run the function [`ObserverConfig::run_observer`].
//!
//! ## Limitations:
//! - Redis PubSub doesn't actually tell us which channels are added or left, instead we receive a notification that "something has changed", and then we have to load the entire set of tracked channels and compare it with which channels are currently joined. This probably causes quite a performance hit.
//! - The Event Subject connection is expected to be blocking, whereas the Database connection is expected to be async.

#![warn(clippy::all, clippy::pedantic, clippy::nursery)]
#![warn(
    warnings,
    future_incompatible,
    nonstandard_style,
    rust_2018_compatibility,
    rust_2018_idioms,
    unused,
    missing_docs,
    missing_copy_implementations,
    missing_debug_implementations,
    trivial_casts,
    trivial_numeric_casts,
    unused_crate_dependencies,
    unused_extern_crates,
    variant_size_differences
)]
#![allow(clippy::module_name_repetitions)]
#![allow(clippy::wildcard_imports)]
#![allow(clippy::missing_errors_doc)]
#![allow(clippy::doc_markdown)]

pub mod observer;
pub mod observer_config;
pub mod redis;

pub use observer::{EventSubject, MsgListener};
pub use observer_config::ObserverConfig;

#[cfg(test)]
use k8s_openapi as _;
