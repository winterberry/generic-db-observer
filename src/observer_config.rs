//! A structure representing a configuration for an observer
//! that includes an event subject and a message broker.

use crate::observer::{EventSubject, MsgListener};
use either::Either;
use std::fmt::Debug;

/// A structure representing a configuration for an observer
/// that includes an event subject and a message broker.
#[derive(Debug)]
pub struct ObserverConfig<ML, ES>
where
    ML: MsgListener,
    ML::Config: Debug,
    ES: EventSubject,
    ES::Config: Debug,
{
    /// Configuration for the event subject.
    pub subject_config: ES::Config,
    /// Configuration for the message broker.
    pub broker_config: ML::Config,
}

impl<ML, ES> ObserverConfig<ML, ES>
where
    ML: MsgListener,
    ML::Config: Debug,
    ES: EventSubject,
    ES::Config: Debug,
{
    /// Provides the configuration for the Event Subject.
    pub const fn subject_config(&self) -> &ES::Config {
        &self.subject_config
    }

    /// Provides the configuration for the Message Listener.
    pub const fn listener_config(&self) -> &ML::Config {
        &self.broker_config
    }

    /// Runs the controller main loop based on the provided configuration.
    pub async fn run_observer(self) -> Result<(), Either<ML::Error, ES::Error>> {
        crate::observer::observer_main_loop(self).await
    }
}
